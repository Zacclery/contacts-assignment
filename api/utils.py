import hashlib

import requests
from PIL import Image
from pandas import read_csv
import base64
from io import BytesIO
from contacts_assignment.settings import CSV_LOCATION, CACHE_DIR
import urllib.request

"""
Validation to consider:
image path exists, and contains image else send None
names - don't validate, could lead to bad user experience
zip alphanumeric plus hyphen and space and less that 12 chars (future proofed slightly) else None
city alphanumeric (apparently there are cases with a number) plus hyphen, apostrophe and space else None (too restrictive?)
"""
# ToDo: dict of functions to clean CSV data when downloading
converter = {
    'street': 0,
    'zip': 0,
    'city': 0,
    'image': 0
}


def get_csv(loc=CSV_LOCATION):
    try:
        return read_csv(loc, encoding="utf-8-sig", converters=converter)
    except:
        return 'CSV file not accessible'


def get_json_from_df(df):
    return df.to_json(orient='records')


def get_cached_image(path):
    try:
        with open(path, "rb") as image_file:
            data = base64.b64encode(image_file.read())
            print(data)
        return data
    except:
        return None


def clean_cache():
    pass


def hash_url(url):
    # hash in order to create unique url reference as file name
    return hashlib.md5(url.encode('utf-8')).hexdigest()


def get_remote_image(uri):
    # return urllib.request.urlretrieve(uri)
    response = requests.get(uri)
    return Image.open(BytesIO(response.content))


def optimize_image(image):
    """ resize image and convert to .png format (if not already) """
    try:
        # import ipdb;ipdb.set_trace()
        # ToDo keep aspect ratio, relevant?
        size = 300, 300 if image.size > (300, 300) else image.size
        new_image = image.resize(size)
        return new_image
    except:
        return None


def convert_image_to_bytes(img, fmt='PNG'):
    buffered = BytesIO()
    img.save(buffered, format=fmt)
    return base64.b64encode(buffered.getvalue())


def set_cache_image(base64_img, path):
    with open(path, 'w') as f:
        f.write(base64_img)
