from django.http import JsonResponse, HttpResponse
from .utils import *
from contacts_assignment.settings import CACHE_DIR
import os


def contact_list(request):
    if request.method == 'GET':
        df = get_csv()
        for url in df['image']:
            try:
                if url:
                    url_hash = hash_url(url)
                    path = os.getcwd()+CACHE_DIR+url_hash+'.png'
                else:
                    # no image URL provided go to next loop
                    continue

                if os.path.isfile(path):
                    # file already cached (will exist and be empty if invalid url etc)
                    contents = get_cached_image(path)
                    df['image'] = contents
                    print(df)
                else:
                    # search online, not cached
                    try:
                        image = get_remote_image(url)
                        png_image = optimize_image(image)
                        bytes_image = convert_image_to_bytes(png_image)
                        set_cache_image(str(bytes_image, 'utf-8'), path)
                        df.loc[url]['image'] = str(bytes_image, 'utf-8')
                        print(df)
                    except:
                        # create empty file for cache
                        open(path, 'a').close()
            except:
                df.replace(url, None)

        json = get_json_from_df(df)
        return JsonResponse(json, safe=False)
    else:
        return HttpResponse('Invalid HTTP Method', status=400)


def contact(request, pk):
    """
    Retrieve individual contact
    Pseudo pk as per order in spreadsheet, purely for demonstration purposes
    """
    # ToDo: Currently not working
    if request.method == 'GET':
        try:
            df = get_csv()
            df = df[int(pk) - 1:int(pk)]
            url = df['image']
            try:
                if url:
                    url_hash = hash_url(url)
                    path = os.getcwd()+CACHE_DIR+url_hash+'.png'
                else:
                    # no image URL provided
                    json = get_json_from_df(df)
                    return JsonResponse(json, safe=False)

                if os.path.isfile(path):
                    # file already cached (will exist and be empty if invalid url etc)
                    contents = get_cached_image(path)
                    print(contents)
                    df.at[url, 'image'] = contents
                else:
                    # search online, not cached
                    try:
                        image = get_remote_image(url)
                        png_image = optimize_image(image)
                        bytes_image = convert_image_to_bytes(png_image)
                        set_cache_image(str(bytes_image, 'utf-8'), path)
                        df.at[url, 'image'] = str(bytes_image, 'utf-8')
                    except:
                        # create empty file for cache
                        open(path, 'a').close()
            except:
                df.replace(url, None)

            response = get_json_from_df(contact)
            return JsonResponse(response, safe=False)
        except:
            return HttpResponse(status=404)
    else:
        return HttpResponse('Invalid HTTP method', status=400)
